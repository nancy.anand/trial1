var a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function binarySearch(arr, keyValue) {
  let startIndex = 0;
  let end = arr.length - 1;

  while (startIndex <= end) {
    let midIndex = Math.floor((startIndex + end) / 2);

    if (arr[midIndex] === keyValue) {
      return midIndex;
    } else if (arr[midIndex] < keyValue) {
      startIndex = midIndex + 1;
    } else {
      end = midIndex - 1;
    }
  }
  return -1;
}

module.exports = binarySearch;

// var result = binarySearch(a, 8);
// console.log(result);

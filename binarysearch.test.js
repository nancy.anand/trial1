const binarySearch = require('./binarysearch');

const arr = [1, 2, 3, 4, 5, 6];
//const arr2 = [5, 7, 11, 12];

describe('Should test binary search program', () => {
  test('Should return the index of 4', () => {
    expect(binarySearch(arr, 4)).toBe(3);
  });

  test.each([
    ['Should return the index of 1', 1, 0],
    ['Should return the index of 2', 2, 1],
    ['Should return the index of 3', 3, 2],
    ['Should return the index of 4', 4, 3],
    ['Should return the index of 5', 5, 4],
    ['Should return the index of 6', 6, 5],
    ['Should return the index of 7', 7, -1]
  ])('%s', (unused_var, number, index) => {
    expect(binarySearch(arr, number)).toBe(index);
  });
});

// describe('Should test Binary Search program for edge cases', () => {

// })

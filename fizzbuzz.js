function fizzbuzz (testNumber) {
  if (typeof testNumber !== 'number') {
    console.log('Invalid Number')
  }
  for (let currentNum = 1; currentNum <= testNumber; currentNum++) {
    if (currentNum % 15 === 0) console.log('FizzBuzz')
    else if (currentNum % 3 === 0) console.log('Fizz')
    else if (currentNum % 5 === 0) console.log('Buzz')
    else console.log(currentNum)
  }
}
module.exports = fizzbuzz

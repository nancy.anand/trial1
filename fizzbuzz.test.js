const fizzbuzz = require('./fizzbuzz')

describe('Should test the fizz buzz program logic', () => {
  test('Should output correct values as per the fizzbuzz logic', () => {
    const spy = jest.spyOn(console, 'log')
    const timesCalled = 25
    fizzbuzz(timesCalled)
    expect(spy).toHaveBeenCalledTimes(timesCalled)
    expect(spy.mock.calls[0][0]).toBe(1)
    expect(spy.mock.calls[24][0]).toBe('Buzz')
  })

  test('Should output correct values as per the fizzbuzz logic', () => {
    const spy = jest.spyOn(console, 'log')
    const timesCalled = 40
    fizzbuzz(timesCalled)
    expect(spy).toHaveBeenCalledTimes(40)
    expect(spy.mock.calls[29][0]).toBe('FizzBuzz')
  })
  beforeEach(() => {
    jest.resetAllMocks()
  })

  test('Should output correct values as per the fizzbuzz logic', () => {
    const spy = jest.spyOn(console, 'log')
    const timesCalled = '.'
    fizzbuzz(timesCalled)
    expect(spy).toHaveBeenCalledTimes(1)
    expect(spy.mock.calls[0][0]).toBe('Invalid Number')
  })
})

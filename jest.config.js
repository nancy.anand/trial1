module.exports = {
  verbose: true,
  collectCoverageFrom: [
    '<rootDir>/*.{js,jsx}',
    '!**/node_modules/**',
    '!<rootDir>/linkedlist.js',
    '!<rootDir>/jest.config.js',
    '<rootDir>/sherlockValidString.js',
    '<rootDir>/fizzbuzz.js'
  ],
  coverageThreshold: {
    global: {
      branches: 95,
      functions: 95,
      lines: 95,
      statements: 95
    }
  },
  coverageReporters: ['json', 'lcov', 'text']
};

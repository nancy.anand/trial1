var assert = require('assert');
class Node {
  constructor(number) {
    this.number = number;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
    this.size = 0;
  }

  insert(number) {
    let node = new Node(number);

    //to store the curr node
    let current;

    if (this.head == null) this.head = node;
    else {
      current = this.head;

      while (current.next) {
        current = current.next;
      }

      current.next = node;
    }
    this.size++;
  }

  printList() {
    let curr = this.head;
    let str = '';
    while (curr) {
      str += curr.number + ' ';
      curr = curr.next;
    }
    console.log(str);
  }

  delete(number) {
    let curr = this.head;
    let prev = null;

    while (curr != null) {
      if (curr.number === number) {
        if (prev == null) {
          this.head = curr.next;
        } else {
          prev.next = curr.next;
        }
        this.size--;
        return curr.number;
      }
      prev = curr;
      curr = curr.next;
    }
    return -1;
  }
}

//CASE 1
let list1 = new LinkedList();

list1.insert(4);
list1.insert(5);
list1.insert(6);
list1.insert(7);
list1.insert(8);
list1.insert(9);

list1.printList();
list1.delete(4);
list1.printList();
list1.delete(6);
list1.printList();
list1.delete(9);
list1.printList();

//CASE 2
let list2 = new LinkedList();

list2.insert(11);
list2.insert(12);
list2.insert(13);
list2.insert(14);
list2.insert(15);
list2.insert(16);

list2.printList();
list2.delete(11);
list2.printList();
list2.delete(13);
list2.printList();
list2.delete(16);
list2.printList();

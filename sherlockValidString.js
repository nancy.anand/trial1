function isValidString(s) {
  let letterCount = {};
  let character;
  for (character of s) {
    letterCount[character] = letterCount[character] + 1 || 1;
  }
  let countDiffChar = 0;
  let countArray = Object.values(letterCount);
  countArray.map(character => {
    if (character !== countArray[0]) countDiffChar++;
    if (character > letterCount[0] && character !== letterCount[0] + 1)
      return 'NO';
    else if (character < letterCount[0] && character !== letterCount[0] - 1)
      return 'YES';
  });

  if (countDiffChar > 1) return 'NO';
  if (countDiffChar === 1) return 'YES';
  return 'YES';
}

///console.log(isValidString);
module.exports = isValidString;

const { test } = require('@jest/globals');
const isValidString = require('./sherlockValidString');
const stringOne = 'aabbccdd';
const stringTwo = 'abc';
const stringThree = 'aabbbccd';
const stringFour = 'aabbcd';

describe('should return whether string is valid or not', () => {
  test('Should return validity of string', () => {
    expect(isValidString(stringOne)).toBe('YES');
  });
  test('Should return validity of string', () => {
    expect(isValidString(stringTwo)).toBe('YES');
  });
  test('Should return validity of string', () => {
    expect(isValidString(stringThree)).toBe('NO');
  });
  test('Should return validity of string', () => {
    expect(isValidString(stringFour)).toBe('NO');
  });
});

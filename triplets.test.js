const comparisonValue = require('./triplets');

const a = [5, 6, 7];
const b = [3, 6, 10];

describe('Should compare and find the comparison points for a[i] and b[i]', () => {
  test.each([
    ['Should return [1,1]', [5, 6, 7], [3, 6, 10], [1, 1]],
    ['Should return [2, 1]', [17, 28, 30], [99, 16, 8], [2, 1]],
    ['Should return [0, 3]', [6, 8, 12], [7, 9, 15], [0, 3]]
  ])('%s', (unused_var, aliceScore, bobScore, comparisonScore) => {
    expect(comparisonValue(aliceScore, bobScore)).toStrictEqual(
      comparisonScore
    );
  });
});
